package by.samarcev;

import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleCalculatorTest {

    @Test
    public void integerRectangleArea() {
        RectangleCalculator rectangleAreaCalculator = new RectangleCalculator();
        int rectangleArea = rectangleAreaCalculator.integerRectangleArea(4, 5);
        int expected = 20;
        assertEquals(rectangleArea, expected);
    }

    @Test
    public void doubleRectangleArea() {
        RectangleCalculator rectangleAreaCalculator = new RectangleCalculator();
        double rectangleArea = rectangleAreaCalculator.doubleRectangleArea(3.5, 5.3);
        double expected = 18.55;
        assertEquals(rectangleArea, expected, 2);
    }

    @Test
    public void integerRectanglePerimeter() {
        RectangleCalculator rectangleCalculator = new RectangleCalculator();
        int rectanglePerimeter = rectangleCalculator.integerRectanglePerimeter(4, 5);
        int expected = 18;
        assertEquals(rectanglePerimeter, expected);
    }

    @Test
    public void doubleRectanglePerimeter() {
        RectangleCalculator rectangleCalculator = new RectangleCalculator();
        double rectanglePerimeter = rectangleCalculator.doubleRectanglePerimeter(4.5, 5.5);
        double expected = 20.00;
        assertEquals(rectanglePerimeter, expected, 2);
    }
}