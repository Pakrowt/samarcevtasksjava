package by.samarcev;

public class RectangleCalculator {

    public int integerRectangleArea(int a, int b) {
        if ((a | b) <= 0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else
            System.out.println(a * b);
        return a * b;
    }

    public double doubleRectangleArea(double a, double b) {
        if (a <= 0.0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else if (b <= 0.0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else
            System.out.println(a * b);
        return a * b;
    }

    public int integerRectanglePerimeter(int a, int b) {
        if ((a | b) <= 0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else
            System.out.println((a + b) * 2);
        return (a + b) * 2;
    }

    public double doubleRectanglePerimeter(double a, double b) {
        if (a <= 0.0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else if (b <= 0.0) {
            throw new ArithmeticException("Wrong value! Try again.");
        } else
            System.out.println((a + b) * 2);
        return (a + b) * 2;
    }
}
