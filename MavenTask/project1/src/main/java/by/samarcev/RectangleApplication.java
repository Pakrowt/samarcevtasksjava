package by.samarcev;

public class RectangleApplication {
    public static void main(String[] args) {
        RectangleCalculator rectangleAreaCalculator = new RectangleCalculator();
        rectangleAreaCalculator.integerRectangleArea(3, 5);
        rectangleAreaCalculator.doubleRectangleArea(4.2, 3.4);
        rectangleAreaCalculator.doubleRectanglePerimeter(4.5, 6.5);
        rectangleAreaCalculator.integerRectanglePerimeter(4, 6);

    }
}
