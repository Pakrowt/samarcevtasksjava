package by.samarcev;

import java.util.Scanner;

public class RectangleAreaCalculator {
    public int integerRectangleArea(int... a) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write first side of the rectangle");
        int side1 = scanner.nextInt();
        System.out.println("Write second side of the rectangle");
        int side2 = scanner.nextInt();
        if ((side1 | side2) <= 0) {
            System.out.println("Wrong value!");
        } else
            System.out.println(side1 * side2);
        return side1 * side2;
    }
}