package by.samarcev;

import org.junit.Test;

import static org.junit.Assert.*;

public class AppTest {

    @Test
    public void rectangleAreaTest() {
        RectangleAreaCalculator rectangleAreaCalculator = new RectangleAreaCalculator();
        int area = rectangleAreaCalculator.integerRectangleArea(3, 5);
        int expected = 15;
        assertEquals(area,expected);

    }
}