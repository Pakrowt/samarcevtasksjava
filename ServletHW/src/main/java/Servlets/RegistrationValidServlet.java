package Servlets;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/reg")
public class RegistrationValidServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        ServletContext servletContext = getServletContext();
        servletContext.setAttribute("login", login);
        servletContext.setAttribute("password", password);
        servletContext.setAttribute("name", name);
//        servletContext.getAttribute("login");
//        servletContext.getAttribute("password");
//        PrintWriter writer = resp.getWriter();
//        writer.println("login :" + login + "\n password : " + password);
        servletContext.getRequestDispatcher("/login.jsp").forward(req,resp);
    }
}
