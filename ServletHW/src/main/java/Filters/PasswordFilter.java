package Filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/validate")
public class PasswordFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init password filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String passwordIn = servletRequest.getParameter("passwordOut");
        String password = (String) servletRequest.getServletContext().getAttribute("password");
        if (passwordIn.equals(password)) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
        servletRequest.getRequestDispatcher("wrongPassword.html").forward(servletRequest, servletResponse);


    }

    @Override
    public void destroy() {
        System.out.println("destroy password filter");
    }
}
