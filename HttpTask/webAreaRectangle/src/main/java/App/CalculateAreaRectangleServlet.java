package App;

import by.samarcev.RectangleAreaCalculator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/area")
public class CalculateAreaRectangleServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("text/html");
        String contentType = "<!DOCTYPE html>\n";
        writer.println(contentType + "<html>\n" + "<body>");
        try {
            double firstSide = Double.parseDouble(req.getParameter("firstSide"));
            double secondSide = Double.parseDouble(req.getParameter("secondSide"));
//            String path = req.getContextPath() + "/error.jsp";
//            RequestDispatcher dispatcher = req.getRequestDispatcher("error.jsp");
//            ServletContext servletContext = getServletContext();
//            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/error.jsp");
//            if (firstSide <= 0.0) {
//                resp.sendRedirect(path);
//                dispatcher.forward(req, resp);
//                requestDispatcher.forward(req, resp);
//            }
//            if (secondSide <= 0.0) {
//                dispatcher.forward(req, resp);
//                resp.sendRedirect(path);
//                requestDispatcher.forward(req, resp);
//            } else {
            RectangleAreaCalculator rectangleAreaCalculator = new RectangleAreaCalculator();
            double areaRectangle = rectangleAreaCalculator.doubleRectangleArea(firstSide, secondSide);
            writer.println("<h2>Площадь прямоугольника равна : " + areaRectangle + "</h2> </body>");
//            }
        } catch (ArithmeticException e) {
        } catch (Exception e) {
            writer.println("<h2> Вы ввели неправильное значение! Попробуйте снова, но используйте только цифры. <h2></body>");

        } finally {
            writer.close();
        }
    }
}
