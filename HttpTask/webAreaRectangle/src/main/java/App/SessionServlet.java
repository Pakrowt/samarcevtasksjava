package App;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/count")
public class SessionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Integer count = (Integer) session.getAttribute("count");
//        session.getAttribute("count",)
        if (count == null) {
            count = 1;
            session.setAttribute("count", count);
        } else {
            session.setAttribute("count", count + 1);

        }
//        String name = req.getParameter("name");
//        String surname = req.getParameter("surname");

        PrintWriter writer = resp.getWriter();
        writer.println("<html>");
//        writer.println("<h1> Hello," + name + " " + surname + "</h1>");
        writer.println("<h1> Your count is :" + count + "</h1>");
        writer.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
