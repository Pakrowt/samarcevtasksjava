package Tags;

import by.samarcev.RectangleCalculator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class AreaCalculateTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        double firstSide = Double.parseDouble(pageContext.getRequest().getParameter("firstSide"));
        double secondSide = Double.parseDouble(pageContext.getRequest().getParameter("secondSide"));
        RectangleCalculator calculator = new RectangleCalculator();
        double areaRectangle = calculator.doubleRectanglePerimeter(firstSide, secondSide);
        JspWriter out = pageContext.getOut();
        try {
            out.print("<h2>");
            out.print(String.format("Площадь прямоугольника равняется : %.1f", areaRectangle));
            out.print("</h2>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
